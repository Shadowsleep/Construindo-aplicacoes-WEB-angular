import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { MenuItem } from '../models/menu-item.model';
import {animate,state,trigger,style,transition} from '@angular/animations'

@Component({
  selector: 'mt-menu-item',
  templateUrl: './menu-item.component.html',
  animations:[
    trigger('mostrarMenu',[
      state('ready',style({opacity:1})),
      transition('void=>ready',[
        style({opacity:0, transform:'translateY(-20px'}),
        animate('500ms 0s ease-in')
      ])
    ])
  ]
})
export class MenuItemComponent implements OnInit {
  menuMostre='ready'
  @Input() menu: MenuItem;
  @Output() addEmit = new EventEmitter();
 
  constructor() { }

  ngOnInit() {
  }
  emitAddMenu() {
    this.addEmit.emit(this.menu);
  }
}
