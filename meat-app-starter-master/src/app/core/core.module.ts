import { NgModule } from "@angular/core";
import { ServicoCart } from "app/shopping-cart/servico-cart.component";
import { Servicos } from "app/service/service";
import { OrderService } from "app/order/order-service";
import { NotificacoesService } from "app/shared/mensagens/notificacoes";


@NgModule({
    providers:[ServicoCart,Servicos,OrderService,NotificacoesService]
})

export class CoreModule{

}