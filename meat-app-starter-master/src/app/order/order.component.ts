import { Component, OnInit } from '@angular/core';
import { RadioOptions } from 'app/shared/radio/radio-options.model';
import { OrderService } from './order-service';
import { CartItem } from 'app/shopping-cart/cart-item.model';
import { Order, OrderItem } from 'app/models/order.model';
import { Router } from '@angular/router'
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms'
@Component({
  selector: 'mt-order',
  templateUrl: './order.component.html'
})
export class OrderComponent implements OnInit {
  delivery: number = 8;
  paymentOptions: RadioOptions[] = [
    { label: 'Dinheiro', value: 'MON' },
    { label: 'Cartão de Crédito', value: 'CRED' },
    { label: 'Vale Refeição', value: 'REF' }
  ];
  orderForm: FormGroup;

  constructor(private orderService: OrderService, private route: Router, private formBuilder: FormBuilder) { }

  emailPartner = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
  numeroPartner = /^[0-9]*$/
  ngOnInit() {
    this.orderForm = this.formBuilder.group({
      name: this.formBuilder.control('', [Validators.required, Validators.minLength(5)]),
      emailConfirmation: this.formBuilder.control('', [Validators.required, Validators.pattern(this.emailPartner)]),
      email: this.formBuilder.control('', [Validators.required, Validators.pattern(this.emailPartner)]),
      address: this.formBuilder.control('', [Validators.required]),
      number: this.formBuilder.control('', [Validators.required, Validators.pattern(this.numeroPartner)]),
      optionalAddress: this.formBuilder.control(''),
      paymentOption: this.formBuilder.control('', [Validators.required])
    }, { validator: OrderComponent.equalsTo })
  }
  static equalsTo(group: AbstractControl): { [key: string]: Boolean } {
    const email = group.get('email')
    const emailConfirmation = group.get('emailConfirmation')
    if (!email || !emailConfirmation) {
      return undefined
    }

    if (email.value != emailConfirmation.value) {
      return { emailsNotMatch: true }
    }
    return undefined
  }
  ItemTotal(): number {
    return this.orderService.totalItem();
  }
  cartItems(): CartItem[] {
    return this.orderService.cartItems();
  }
  incrementaItem(cart: CartItem) {
    this.orderService.incrementaItem(cart);
  }
  decrementaItem(cart: CartItem) {
    this.orderService.decrementaItem(cart);
  }
  removeItem(cart: CartItem) {
    this.orderService.removeItem(cart);
  }
  fecharPedido(pedido: Order) {
    pedido.orderItem = this.cartItems().map((item: CartItem) => new OrderItem(item.quantidade, item.itemMenu.id));
    this.orderService.fecharPedido(pedido).subscribe((orderId: Order) => {
      this.route.navigate(['/order-sumary']);
      this.orderService.clear();

    });
  }
  
}
