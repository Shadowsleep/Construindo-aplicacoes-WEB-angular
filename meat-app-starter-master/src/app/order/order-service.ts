import { Injectable } from "@angular/core";
import { ServicoCart } from "app/shopping-cart/servico-cart.component";
import { CartItem } from "app/shopping-cart/cart-item.model";
import { Order } from "app/models/order.model";
import { Observable } from "rxjs/Observable";
import { Http, RequestOptions, Headers } from "@angular/http";
import { conexao } from "app/conections/app.api";
@Injectable()
export class OrderService {


    //esse metodo é um proxi entre o order e ServicoCart
    constructor(private cartService: ServicoCart, private http: Http) {

    }
    totalItem(): number {
        return this.cartService.total();
    }
    cartItems() {
        return this.cartService.items;
    }
    incrementaItem(cart: CartItem) {
        this.cartService.incrementaItem(cart);
    }
    decrementaItem(cart: CartItem) {
        this.cartService.decrementaItem(cart);
    }
    removeItem(cart: CartItem) {
        this.cartService.removeItem(cart);
    }

    clear(): any {
        this.cartService.clear();
    }

    fecharPedido(pedido: Order): Observable<Order> {
        return this.http.post(`${conexao}/orders`,
            JSON.stringify(pedido),
            this.headers(),
        ).map(retorno => retorno.json());
    }
    headers(): RequestOptions {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return new RequestOptions({ headers: headers })

    }
}