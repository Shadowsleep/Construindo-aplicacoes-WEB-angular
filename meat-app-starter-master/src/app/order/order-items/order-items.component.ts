import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CartItem } from 'app/shopping-cart/cart-item.model';

@Component({
  selector: 'mt-order-items',
  templateUrl: './order-items.component.html'
})
export class OrderItemsComponent implements OnInit {

  @Input() items: CartItem[];
  @Output() incrementaItem = new EventEmitter<CartItem>();
  @Output() decrementaItem = new EventEmitter<CartItem>();
  @Output() removeItem = new EventEmitter<CartItem>();
  constructor() { }

  ngOnInit() {
  }

  emitIncrementaItem(item: CartItem) {
    this.incrementaItem.emit(item);
  }
  emitDecrementaItem(item: CartItem) {
    this.decrementaItem.emit(item);
  }
  emitRemoveItem(item: CartItem) {
    this.removeItem.emit(item);
  }

}
