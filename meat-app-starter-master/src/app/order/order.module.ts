import { NgModule } from "@angular/core";
import { OrderComponent } from "./order.component";
import { DeliveryCostComponent } from "./delivery-cost/delivery-cost.component";
import { OrderItemsComponent } from "./order-items/order-items.component";
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "app/shared/shared.module";


const ROTAS:Routes=[
    {path:'',component:OrderComponent}
]
@NgModule({
    declarations: [OrderComponent,DeliveryCostComponent,OrderItemsComponent],
    imports: [SharedModule,RouterModule.forChild(ROTAS)],
})
export class OrderModule {

}