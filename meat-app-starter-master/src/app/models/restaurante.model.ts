export interface Restaurante {
    id: number,
    name: string,
    category: string,
    deliveryEstimate: number,
    rating: number,
    imagePath: string,
    about?: string,
    hours?: string
}
