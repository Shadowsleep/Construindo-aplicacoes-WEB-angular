class Order {
    constructor(public endereco:string,
        public numero:number,
        public complemento:string,
        public paymentOption:string,
        public orderItem:OrderItem[]){

    }
}
class OrderItem{
    constructor(public quantidade:number, public menuId:string){

    }
}
export{Order,OrderItem}