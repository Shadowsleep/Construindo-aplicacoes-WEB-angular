import { CartItem } from "./cart-item.model";
import { MenuItem } from "../models/menu-item.model";
import { Injectable } from "@angular/core";
import { NotificacoesService } from "app/shared/mensagens/notificacoes";


@Injectable()
export class ServicoCart {

    constructor(private notificacoes:NotificacoesService){

    }

    items: CartItem[] = []

    addItem(item: MenuItem) {
        let existe = this.items.find(x => x.itemMenu.id == item.id);
        if (existe) {
            this.incrementaItem(existe)
        } else {
            this.items.push(new CartItem(item));
        }
        this.notificacoes.Notificar(`Você Adicionou o item ${item.name}`);
    }
    incrementaItem(item: CartItem) {
        item.quantidade = item.quantidade + 1;
    }
    decrementaItem(item: CartItem) {
        item.quantidade = item.quantidade - 1;
        if (item.quantidade == 0) {
            this.removeItem(item);
        }
    }

    removeItem(item: CartItem) {
        this.items.splice(this.items.indexOf(item), 1)
        this.notificacoes.Notificar(`Você Removeu o item ${item.itemMenu.name}`);
    }
    clear() {
        this.items = [];
    }
    total(): number {
        return this.items.map(x => x.valor()).reduce((prev, value) => prev + value, 0);
    }
}