import { MenuItem } from "../models/menu-item.model";

export class CartItem{
    constructor (public itemMenu:MenuItem, public quantidade:number=1){

    }
    valor():number{
        return this.itemMenu.price*this.quantidade;
    }
}