import { EventEmitter } from "@angular/core";

export class NotificacoesService{
    notifique =new EventEmitter<any>();

    Notificar(msgm:string){
        this.notifique.emit(msgm); 
    }
}