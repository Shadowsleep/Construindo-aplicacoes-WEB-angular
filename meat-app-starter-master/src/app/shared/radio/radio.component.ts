import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { RadioOptions } from './radio-options.model';
import { NG_VALUE_ACCESSOR,ControlValueAccessor } from '@angular/forms';

@Component({
  selector: 'mt-radio',
  templateUrl: './radio.component.html',
  providers:[ 
    {
      provide:NG_VALUE_ACCESSOR,
      useExisting:forwardRef(()=>RadioComponent),
      multi:true
    }
  ]
})
export class RadioComponent implements OnInit, ControlValueAccessor {
  @Input() options: RadioOptions[];
  value: any
  onChange: any;

  constructor() { }

  ngOnInit() {
  }
  setValue(valor: any) {
    this.value = valor;
    //tem q ser o valor interno do componente.
    this.onChange(this.value);
  }
  // olhar a documentação do metodo ControlValueAccessor vide aula 64
  writeValue(obj: any): void {
    this.value = obj;
  }
  registerOnChange(fn: any): void {
   this.onChange= fn;
  }
  registerOnTouched(fn: any): void {
   
  }
  setDisabledState?(isDisabled: boolean): void {
   
  }

}
