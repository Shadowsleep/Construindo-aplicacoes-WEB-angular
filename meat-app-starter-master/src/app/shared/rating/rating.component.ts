import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'mt-rating',
  templateUrl: './rating.component.html'
})
export class RatingComponent implements OnInit {
  rates: number[] = [1, 2, 3, 4, 5]
  rate: number = 0;
  temporario: number;
  @Output() rateEmit=new EventEmitter<number>();
  constructor() { }

  ngOnInit() {
  }
  setRate(r: number) {
    this.rate = r;
    this.temporario = undefined;
    this.rateEmit.emit(this.rate);
  }
  setTemporatyRate(r: number) {
    if (this.temporario === undefined) {
      this.temporario = this.rate;
    }
    this.rate = r;
  }
  clearTemporary(r: number) {
    if (this.temporario !== undefined) {
      this.rate = this.temporario;
      this.temporario = undefined;
    }
  }
}
