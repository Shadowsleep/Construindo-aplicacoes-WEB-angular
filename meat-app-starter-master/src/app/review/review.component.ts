import { Component, OnInit } from '@angular/core';
import { Servicos } from '../service/service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'mt-review',
  templateUrl: './review.component.html'
})
export class ReviewComponent implements OnInit {
  review: Observable<any>

  constructor(private servico: Servicos, private route: ActivatedRoute) { }

  ngOnInit() {
    this.review = this.servico.ReviewRestaurante(this.route.parent.snapshot.params['id'])
  }

}
