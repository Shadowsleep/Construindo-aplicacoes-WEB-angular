import { Component, OnInit } from '@angular/core';
import { Restaurante } from '../models/restaurante.model';
import { ActivatedRoute } from '@angular/router';
import { Servicos } from '../service/service';

@Component({
  selector: 'mt-restaurant-datail',
  templateUrl: './restaurant-datail.component.html'
})
export class RestaurantDatailComponent implements OnInit {

  restaurante: Restaurante

  constructor(private servico: Servicos, private route: ActivatedRoute) { }

  ngOnInit() {
    
    this.servico.GetById(this.route.snapshot.params['id']).subscribe(r => this.restaurante = r);
  }

}
