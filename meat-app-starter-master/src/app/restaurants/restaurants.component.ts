import { Component, OnInit } from '@angular/core';
import { Restaurante } from '../models/restaurante.model';
import { Servicos } from '../service/service';

@Component({
  selector: 'mt-restaurants',
  templateUrl: './restaurants.component.html'
})
export class RestaurantsComponent implements OnInit {

  restaurantes: Restaurante[];
  constructor(private service: Servicos) { }
  ngOnInit() {
    this.service.ListarTodos().subscribe(r => this.restaurantes = r);
  }
}
