import { Routes } from "../../node_modules/@angular/router";
import { HomeComponent } from "./home/home.component";
import { RestaurantsComponent } from "./restaurants/restaurants.component";
import { RestaurantDatailComponent } from "./restaurant-datail/restaurant-datail.component";
import { MenuComponent } from "./menu/menu.component";
import { ReviewComponent } from "./review/review.component";
import { OrderSumaryComponent } from "./order-sumary/order-sumary.component";

export const ROTAS: Routes = [
    { path: "", component: HomeComponent },
    { path: "about", loadChildren: './about/about.module#AboutModule' },
    { path: "restaurants", component: RestaurantsComponent },
    {
        path: "restaurants/:id", component: RestaurantDatailComponent,
        children: [
            { path: '', redirectTo: 'menu', pathMatch: 'full' },
            { path: 'menu', component: MenuComponent },
            { path: 'review', component: ReviewComponent }
        ]
    },
    { path: "order", loadChildren: './order/order.module#OrderModule' },
    { path: "order-sumary", component: OrderSumaryComponent }
];