import { Injectable } from '@angular/core'
import { Http } from '@angular/http'
import { conexao } from '../conections/app.api';
import { Observable } from '../../../node_modules/rxjs/Observable';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'
import { ErrorHandler } from 'app/service/error-handler';
import { Restaurante } from '../models/restaurante.model';
import { MenuItem } from '../models/menu-item.model';

@Injectable()
export class Servicos {

    constructor(private http: Http) { }

    //metodo so tipo observable<any> que usa o http para fazer um get <list>
    ListarTodos(): Observable<any> {
        return this.http.get(`${conexao}/restaurants`)
            .map(Response => Response.json())
            .catch(ErrorHandler.handlerError)
    }
    GetById(id: string): Observable<Restaurante> {
        return this.http.get(`${conexao}/restaurants/${id}`)
            .map(retorno => retorno.json())
            .catch(ErrorHandler.handlerError)
    }
    ReviewRestaurante(id: string): Observable<any> {
        return this.http.get(`${conexao}/restaurants/${id}/reviews`)
            .map(retorno => retorno.json())
            .catch(ErrorHandler.handlerError)
    }
    MenuRestaurante(id: string): Observable<MenuItem[]> {
        return this.http.get(`${conexao}/restaurants/${id}/menu`)
            .map(retorno => retorno.json())
            .catch(ErrorHandler.handlerError)
    }

} 