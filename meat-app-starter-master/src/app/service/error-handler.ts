import { Response } from "../../../node_modules/@angular/http";
import { Observable } from "../../../node_modules/rxjs/Observable";
import "rxjs/add/Observable/throw"
export class ErrorHandler{
 static handlerError(error:Response|any){
     let retorno="";
    if(error instanceof Response){
           retorno=`Erro${error.status} ao obter a url${error.url} - ${error.statusText}` 
    }else{
        retorno=error.toString();
    }
     return Observable.throw(retorno);
 }

}