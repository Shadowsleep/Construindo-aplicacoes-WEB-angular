import { Component, OnInit,} from '@angular/core';
import { MenuItem } from '../models/menu-item.model';

import { Servicos } from '../service/service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
@Component({
  selector: 'mt-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent implements OnInit {

  menus:Observable<MenuItem[]>;

  constructor(private servico: Servicos, private route: ActivatedRoute) { }

  ngOnInit() {
    this.menus = this.servico.MenuRestaurante(this.route.parent.snapshot.params['id'])
  }
  adicionar(event:any){
    
  }
}
